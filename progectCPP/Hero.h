//
// Created by Inna on 12.12.2018.
//

#ifndef PROGECTCPP_HERO_H
#define PROGECTCPP_HERO_H

#include "Unit.h"
#include "Item.h"
#include "Monster.h"
#include "Flask.h"
#include <array>


class Hero: public Unit {
private:
    std::array<Item*, 5>m_items;
    std::array<Item*, 15>m_bag;
    std::array<Item*, 10>m_belt;
public:
    Hero(std::string name, int gold, int hp, int attack, int armor);
    ~Hero();
    std::string getName();
    void setName();
    virtual int setHp() = 0;
    //int setHp(int hp);
    //int getAttack(int attack);
    int getArmor();
    void printInfo();
    void fight(Monster* withHero);
    void useFlask(Flask* which);


};


#endif //PROGECTCPP_HERO_H
