//
// Created by Inna on 12.12.2018.
//

#ifndef PROGECTCPP_SHOP_H
#define PROGECTCPP_SHOP_H

#include <array>
#include "Item.h"
#include "Flask.h"
#include "Unit.h"


class Shop: public Item{
public:
    std::array<Item*,10>m_item;
    std::array<Item*, 10>m_flask;

    void printInfo();

   Shop();

};


#endif //PROGECTCPP_SHOP_H
