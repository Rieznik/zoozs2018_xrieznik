//
// Created by Inna on 12.12.2018.
//

#ifndef PROGECTCPP_MONSTER_H
#define PROGECTCPP_MONSTER_H


#include "Item.h"
#include "Unit.h"

class Monster : public Unit {
private:
    int m_attack;
    Item *m_item;
public:;
Monster(int attack);
    int getAttack();
    ~Monster();


};


#endif //PROGECTCPP_MONSTER_H
