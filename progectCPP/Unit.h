//
// Created by Inna on 12.12.2018.
//

#ifndef PROGECTCPP_UNIT_H
#define PROGECTCPP_UNIT_H


#include <iostream>

class Unit {
public:
    std::string m_name;
    int m_gold;
    int m_lvl;
    int m_hp;
    int m_attak;
    int m_exp;
    int m_armor;

    Unit(std::string name, int gold, int lvl, int hp, int attak, int exp, int armor);
    Unit();

    virtual std::string  getName() = 0;
    virtual void setName() = 0;

    virtual int getGold() = 0;
    virtual int getLvl() = 0;

    virtual int getHp() = 0;
    virtual int setHp() = 0;

   virtual int getAttack() = 0;
   virtual int getExp() = 0;
    virtual int getArmor() = 0;
    virtual void printInfo() = 0;
};


#endif //PROGECTCPP_UNIT_H
