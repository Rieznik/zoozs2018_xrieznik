//
// Created by Inna on 16.12.2018.
//

#ifndef PROGECTCPP_ENGYNE_H
#define PROGECTCPP_ENGYNE_H


class Engyne {
    void startGame();
    int printGold();
    void buyItem();
    int checkHp();
    int buyFlask();
    void fight();
    void gameOver();

};


#endif //PROGECTCPP_ENGYNE_H
