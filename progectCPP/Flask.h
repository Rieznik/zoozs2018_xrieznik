//
// Created by Inna on 16.12.2018.
//

#ifndef PROGECTCPP_FLASK_H
#define PROGECTCPP_FLASK_H


#include <string>

class Flask {
    std::string m_name;
    int m_cost;
    int m_bonus;

    Flask();
    std::string getName();
    int getBonus();
    int getCost();



};


#endif //PROGECTCPP_FLASK_H
