//
// Created by Inna on 12.12.2018.
//

#ifndef PROGECTCPP_ITEM_H
#define PROGECTCPP_ITEM_H


#include <iostream>

class Item {
public:
   std::string m_name;
   int m_cost;
   int m_type;
   int m_bonusArmor;
   int m_bonusAttack;
   int m_item;

   Item(std::string name, int cost, int type, int bonusArmor, int bonusAttack, int item){

   }
   void printInfo();
   int getArmor();
   int getAttack();
  int getType();

};


#endif //PROGECTCPP_ITEM_H
